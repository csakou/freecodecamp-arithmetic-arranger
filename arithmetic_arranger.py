def arithmetic_arranger(problems, result=False):
  
  if (len(problems) > 5):
    return "Error: Too many problems."

  line1 = ""
  line2 = ""
  line3 = ""
  line4 = ""
  arranged_problems = ""
  problems_list = []

  for problem in problems:
    problems_list.append(problem.split(' '))
  
  for problem in problems_list:
    value = 0
    if problem[1] in ['+', '-']:
      if problem[0].isnumeric() and problem[2].isnumeric():
        if (digit_count(int(problem[0])) <= 4) and (digit_count(int(problem[2])) <= 4):
          
          max_dig = max(digit_count(int(problem[0])), digit_count(int(problem[2]))) + 2
          
          line1 += ' '*(max_dig - digit_count(int(problem[0]))) + problem[0] + ' '*4
          line2 += problem[1] + ' '*(max_dig - digit_count(int(problem[2])) - 1) + problem[2] + ' '*4
          line3 += '-'*max_dig + ' '*4

          if result:
            if (problem[1] == "+"):
              value = int(problem[0]) + int(problem[2])
            else:
              value = int(problem[0]) - int(problem[2])
            
            if value > 0:
              line4 += ' '*(max_dig - digit_count(abs(value))) + str(value) + ' '*4
            else:
              line4 += ' '*(max_dig - digit_count(abs(value)) - 1) + str(value) + ' '*4

        else:
          return "Error: Numbers cannot be more than four digits."
      else:
        return "Error: Numbers must only contain digits."
    else:
      return "Error: Operator must be '+' or '-'."
  
  arranged_problems = line1.rstrip() + '\n' + line2.rstrip() + '\n' + line3.rstrip()

  if result:
    arranged_problems += '\n' + line4.rstrip()

  return arranged_problems


def digit_count(num):
  """
    Method taking an integer and calculating how many digits it has.

    @param num int
  """
  number = num
  digit_count = 0

  while number > 0:
    number = number//10
    digit_count += 1

  return digit_count